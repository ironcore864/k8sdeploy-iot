## Mongodb

```
helm install mongodb --set mongodbRootPassword=e9Fs3LDJ stable/mongodb
```

DB/User:

```
use k8s_iot_devices
db.createUser(
  {
    user: "k8s_iot_user",
    pwd: "123456",
    roles: [
       { role: "readWrite", db: "k8s_iot_devices" }
    ]
  }
)
```

CLI: 

```
export MONGODB_ROOT_PASSWORD=$(kubectl get secret --namespace default mongodb -o jsonpath="{.data.mongodb-root-password}" | base64 --decode)
kubectl run --namespace default mongodb-client --rm --tty -i --restart='Never' --image docker.io/bitnami/mongodb:4.2.4-debian-10-r0 --command -- mongo admin --host mongodb --authenticationDatabase admin -u root -p $MONGODB_ROOT_PASSWORD
```

